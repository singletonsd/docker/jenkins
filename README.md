# Singleton SD - Jenkins

Contains necessary files to build a docker image with jenkins inside.

## Commands

### Start

docker-compose up

### Build

docker-compose build

## TODO

[] https://code-maze.com/ci-jenkins-docker/
[] Approfondisci stash command: https://antonyderham.me/post/jenkins-docker-pipelines/

### More about Jenkins in docker

https://github.com/shazChaudhry/docker-jenkins/blob/ee0f386fd1706829b956cb2e723c0f2935496933/Dockerfile

### Docker integration

https://medium.com/@manav503/how-to-build-docker-images-inside-a-jenkins-container-d59944102f30

----------------------

© [Singleton](http://singletonsd.com), Italy, 2019.
